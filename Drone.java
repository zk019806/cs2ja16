/**
 * 
 */
package dronesimulation;


/**
* @author romarioalula
*
*/
public class Drone {
    private int x, y;
    private int id;
    private static int droneCount = 0;
    Drone(int x, int y) {
    	this.x = x;
    	this.y = y;
    	droneCount = droneCount + 1;
    	this.id = droneCount; // this is because new Drone is added so new id is created 
    	
    	// this.id = UUID.randomUUID().toString(); // generates universally
    	//unique identifier randomly 
    }
    
    //methods
    
    public boolean isHere(int sx, int sy) {
    	return this.x ==sx && this.y ==sy;
    	
    }
    
    public String getPos() {
    	return this.getX() + ", " + this.getY();
    	
    	}
    
    private int getX() {
    	return this.x;
    }
    private int getY() {
    	return this.y;
    }
    public int getID() {
    	return this.id;
    }
    public void setPros(int x, int y) {
    	this.x = x;
    	this.y = y;
    	
    }
    
    public String toString() {
    	return "Drone " + this.getID() + " is at " + this.getPos() + "\n";
    	/* suitable constructor and a toString method capable of generating a string 
    	 of the form "Drone 0 is at 5, 3" for example  
    	 */
    }
   
	public static void main(String[] args) {
		Drone a = new Drone(5,3); //create drone
		Drone b = new Drone(7,9);
		System.out.println(a.toString()); //print where is
		System.out.println(b.toString());
		// TODO Auto-generated method stub

	}

}