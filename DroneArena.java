/**
 * 
 */
package dronesimulation;

import java.util.ArrayList;
import java.util.Random;

/**
 * 
 * @author romarioalula
 *
 */
public class DroneArena {
	
	private int length, height;
	private ArrayList<Drone> drones;
	private Random randomGenerator;
	
	public DroneArena(int x, int y) {
		this.setSize(x, x);
		this.drones = new ArrayList<Drone>();
		this.randomGenerator = new Random();
		
	}
	//This generates a new drone at a random position in the arena 
	//utilises a 'do while' loop to ensure the drones are in unique positions 
	
	public void addDrone() {
		int x, y;
		do {
			x = randomGenerator.nextInt(this.length + 1);
			y = randomGenerator.nextInt(this.height + 1);
			
		}while (this.getDroneAt(x,y));
		this.drones.add(new Drone(x,y)); //adds new drone in a different position than the one added before
					
	}
	
	//used to put drone in a set position 
	//checks if a drone is trying to be placed in the same position as another
	//if it tries placing a drone in the same position as another it will stop
	public void addDrone(int x, int y) {
		if (this.getDroneAt(x,y)) {
			System.out.println("Error. Another drone is currently present, therefore cannot position drone.");
			return;
		}
		this.drones.add(new Drone(x,y));
	
	}
	//checks to see if drone is in the arena
	//returns false if it's not and true if it is
	public boolean getDroneAt(int x, int y) {
		for(Drone drone : drones) {
			if (drone.isHere(x, y)) {
				return true;
			}
		}
		return false;
	}
	//Iterating the drone array list
	public String toString() {
		
		StringBuilder droneLocation = new StringBuilder();
		for (Drone drone: drones) {
			droneLocation.append(drone.toString());
		}
		return "Arena size is: " + this.getSize() + "\n" + droneLocation;
	}
	public String getSize() {
		return this.getLength() + "," + this.getLength();
	
	}
	public int getLength() {
		return this.length;
	}
	public int getHeight() {
		return this.height;
	}
	
	public void setSize (int x, int y) {
		
		setlength(x);
		setHeight(y);
		
	}
	private void setlength(int x) {
		this.length = x;
	}
	
	private void setHeight(int y) {
		this.height = y;
	}
	
	public static void main(String[] args) {
		DroneArena p = new DroneArena(20, 10);	// create drone arena
		p.addDrone();
		p.addDrone(10,10);
		p.addDrone(10,10);
		p.addDrone();
		System.out.println(p.toString());	// print where is
	}

}