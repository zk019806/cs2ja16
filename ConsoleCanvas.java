/**
 * 
 */
package dronesimulation;

import java.lang.StringBuilder;
import java.util.Arrays;
/**
 * 
 * @author romarioalula
 *
 */


/** Create the class, include a 2D array as data. Write the constructor which creates the array
 * of the appropriate size and fills in the # and spaces as appropriate. Write the showIt method
 * which puts D at the correct location.
 * Then write the toString method and test it.
 * @author romarioalula
 *
 */
public class ConsoleCanvas {
	
	private char[][] canvas;
	DroneArena d_Arena;
	
	public ConsoleCanvas(int sx, int sy) { //the constructor that creates the array of the appropriate size
		
		d_Arena = new DroneArena(sy, sx);
		this.canvas = new char[sy+2][sx+2];
		Arrays.fill(this.canvas[0], '#');
		Arrays.fill(this.canvas [this.canvas.length-1], '#');
		for (int i = 1; i< this.canvas.length-1; i++) {
			for (int h = 0; h < this.canvas[i].length; h++ ) {
				if (h == 0 || h == this.canvas[i].length-1) {
					this.canvas [i][h] = '#';
					
				}
				else {
					this.canvas[i][h] = ' ';	
				}
			}
		}
	}
	
	public String toString () {
		StringBuilder cDisplay = new StringBuilder ();
		for (int i = 0; i < this.canvas.length; i++) {
			cDisplay.append(this.canvas[i]);
			cDisplay.append('\n');
			
		}
		return cDisplay.toString();
	}
	
	public void showIt(int sx, int sy, char c) {
		
		d_Arena.addDrone(sx,sy);
		for (int i = 0; i < this.canvas.length; i++) {
			for (int h = 0; h < this.canvas[i].length; h++) {
				if (d_Arena.getDroneAt(i-1, h-1)) {
					this.canvas[i][h] = c;
				}
			}
		}
	}
	
	/**
	 * @param args
	 */
	
	public static void main(String[]args) {
		ConsoleCanvas c = new ConsoleCanvas (15, 5); //creates a canvas
		c.showIt(0, 0, 'D');
		c.showIt(3, 8, 'D');
		c.showIt(2, 6, 'D');
		System.out.println(c.toString()); 
	}
	

}
